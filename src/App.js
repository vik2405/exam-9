import React, { Component } from 'react';
import './App.css';
import {connect} from 'react-redux';
import Modal from './components/Modal/Modal';
import { Route, Switch } from 'react-router-dom';
import Contacts from './components/Contacts/Contacts';
import NewContact from './components/NewContact/NewContact';
import Header from './components/Header/Header';



class App extends Component {


  purchaseHandler = () => {
    this.setState({purchasing: true});
};
state = {
  purchasing: false,
};
purchaseCancelHandler = () => {
    this.setState({purchasing: false});
};



  render() {
    return (
      <div className="App">
                <Modal
                    show={this.state.purchasing}
                    title="Some"
                    closed={this.purchaseCancelHandler}>
                    <p className="modal">{this.props.name}</p>
                    <button className="edit" onClick={this.editContact}>Edit</button>  <button className="rem" onClick={this.deleteContact}>Delete</button>
                </Modal>
                <Header />
                <Switch>
                    <Route path="/" exact component={Contacts}/>
                    <Route path="/Add new contact" component={NewContact}/>
                </Switch>
                <button className="open" onClick={this.purchaseHandler}>Open info for user</button>
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => {
  return {

  };

};

 export default connect(mapStateToProps,mapDispatchToProps)(App);

//  {this.props.url}
//  {this.props.name}



