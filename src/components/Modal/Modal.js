import React, {Fragment} from 'react';
import './Modal.css';

const Modal = props => (
    <Fragment>
        <div className="Modal" style={{
            transform: props.show ? 'translateY(0)' : 'translateY(-100vh',
            opasity: props.show ? '1' : '0'
        }}>
            <button className="close" onClick={props.closed}>close</button>
            <h1>{props.title}</h1>
            {props.children}
        </div>
    </Fragment>
);

export default Modal;