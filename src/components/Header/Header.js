import React from 'react';
import {NavLink} from 'react-router-dom'
import './Header.css';
const Header = () => {
    return (
        <header>
            <div className="Navigation">
            <nav className="nav">
                <ul className="ul">
                    <li><NavLink to="/" exact activeClassName="active">Contacts</NavLink></li>
                    <li><NavLink to="/Add new contact" activeClassName="active">Add new contact</NavLink></li>
                </ul>
            </nav>
            </div>
        </header>
    )
};

export default Header;